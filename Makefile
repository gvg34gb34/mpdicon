# surf - simple browser
# See LICENSE file for copyright and license details.

include config.mk

SRC = mpdicon.c
OBJ = ${SRC:.c=.o}

all: options mpdicon

options:
	@echo mpdicon build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	@echo CC $<
	@${CC} -c ${CFLAGS} $<

${OBJ}: config.mk


mpdicon: ${OBJ}
	@echo CC -o $@
	@${CC} -o $@ mpdicon.o ${LDFLAGS}

clean:
	@echo cleaning
	@rm -f mpdicon ${OBJ} mpdicon-${VERSION}.tar.gz

dist: clean
	@echo creating dist tarball
	@mkdir -p mpdicon-${VERSION}
	@cp -R CHANGELOG Makefile README config.mk ${SRC} mpdicon-${VERSION}
	@tar -cf mpdicon-${VERSION}.tar mpdicon-${VERSION}
	@bzip2 mpdicon-${VERSION}.tar
	@rm -rf mpdicon-${VERSION}

install: all
	@echo installing executable file to ${DESTDIR}${PREFIX}/bin
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f mpdicon ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/mpdicon

uninstall:
	@echo removing executable file from ${DESTDIR}${PREFIX}/bin
	@rm -f ${DESTDIR}${PREFIX}/bin/mpdicon

.PHONY: all options clean dist install uninstall
